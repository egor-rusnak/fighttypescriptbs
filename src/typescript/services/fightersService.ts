import { callApi } from '../helpers/apiHelper';
import { Fighter } from '../models/fighter';

export async function getFighters() {
  try {
    const endpoint: string = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');

    return apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: string) {
  const fighter = await callApi(`details/fighter/${id}.json`, 'GET');
  return fighter as Fighter;
}

