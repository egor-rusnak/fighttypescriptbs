import { createElement } from './helpers/domHelper';
import { Fighter } from './models/fighter';

export function createFighter(fighter: Fighter, handleClick: Function, selectFighter: Function) {
  const { name, source } = fighter;
  const nameElement = createName(name);
  const imageElement = createImage(source);
  const checkboxElement = createCheckbox();
  const fighterContainer = createElement('div', 'fighter');

  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: Event) => ev.stopPropagation();
  const onCheckboxClick = async (ev: Event) => await selectFighter(ev, fighter);
  const onFighterClick = (ev: Event) => handleClick(ev, fighter);

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick, false);

  return fighterContainer;
}

export function createName(name: string) {
  const nameElement = createElement('span', 'name');
  nameElement.innerText = name;

  return nameElement;
}

export function createImage(source: string) {
  const attributes = { src: source };
  const imgElement = createElement('img', 'fighter-image', attributes);

  return imgElement;
}

export function createCheckbox() {
  const label = createElement('label', 'custom-checkbox');
  const span = createElement('span', 'checkmark');
  const attributes = { type: 'checkbox' };
  const checkboxElement = createElement('input', '', attributes);

  label.append(checkboxElement, span);
  return label;
}