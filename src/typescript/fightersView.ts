import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { Fighter } from './models/fighter';
import { getFighterDetails } from './services/fightersService';
import { showModal } from './modals/modal';
import * as PageActions from './app';

export function createFighters(fighters: Fighter[]) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement('div', 'fighters');

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map<string, Fighter>();

async function showFighterDetails(event: Event, fighter: Fighter) {
  PageActions.startLoading();
  if (!fightersDetailsCache.has(fighter._id))
    fightersDetailsCache.set(fighter._id, await getFighterInfo(fighter._id));

  PageActions.stopLoading();
  showFighterDetailsModal(fightersDetailsCache.get(fighter._id));
}

export async function getFighterInfo(fighterId: string): Promise<Fighter> {
  const fighter = await getFighterDetails(fighterId);
  return fighter;
}

function createFightersSelector() {
  const selectedFighters = new Map<string, Fighter>();

  return async function selectFighterForBattle(event: Event, fighter: Fighter) {
    PageActions.startLoading();
    PageActions.lockCheckboxes();
    const fullInfo = await getFighterInfo(fighter._id)

    if ((event.target as HTMLInputElement).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else {
      selectedFighters.delete(fighter._id);
    }

    PageActions.stopLoading();
    if (selectedFighters.size === 2) {
      const fighters = selectedFighters.values();
      const first = fighters.next().value as Fighter;
      const second = fighters.next().value as Fighter;
      try {
        const winner = await fight(first, second);
        showWinnerModal(winner);
      } catch (error) {
        const errorBody = createElement('p','fight-log-item');
        errorBody.innerText = error.message;
        showModal("Fight is over!", errorBody);
      }
    }    
    PageActions.unlockCheckboxes();
  }
}
