import { createElement } from '../helpers/domHelper';

export function showModal(title: string, bodyElement: HTMLElement, closableByUser: boolean = true) {
  const root = getModalContainer();
  const modal = createModal(title, bodyElement, closableByUser);

  root.append(modal);
}

function getModalContainer() {
  return document.getElementById('root');
}

function createModal(title: string, bodyElement: HTMLElement, closableByUser: boolean) {
  const layer = createElement('div', 'modal-layer');
  const modalContainer = createElement('div', 'modal-root');
  const header = createHeader(title, closableByUser);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: string, hasCloseButton: boolean) {
  const headerElement = createElement('div', 'modal-header');
  const titleElement = createElement('span');

  titleElement.innerText = title;
  headerElement.append(title);
  if (hasCloseButton) {
    const closeButton = createElement('div', 'close-btn');
    closeButton.innerText = '×';
    closeButton.addEventListener('click', hideModal);
    headerElement.append(closeButton);
  }

  return headerElement;
}

export function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
