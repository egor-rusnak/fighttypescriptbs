import { createImage } from '../fighterView';
import { createElement } from '../helpers/domHelper';
import { Fighter } from '../models/fighter';
import { showModal } from './modal';

export function showFighterDetailsModal(fighter: Fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal(title, bodyElement);
}

function createFighterDetails(fighte: Fighter) {

  const fighterDetails = createElement('div', 'modal-body');
  const nameElement = createElement('span', 'fighter-info');
  const attackElement = createElement('span', 'fighter-info');
  const defenseElement = createElement('span', 'fighter-info');
  const healthElement = createElement('span', 'fighter-info');
  const imageElement = createImage(fighte.source);

  nameElement.innerText = `Name: ${fighte.name}`;
  attackElement.innerText = `Attack: ${fighte.attack} points`;
  defenseElement.innerText = `Defence: ${fighte.defense} points`;
  healthElement.innerText = `Health: ${fighte.health} points`;

  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(imageElement);

  return fighterDetails;
}
