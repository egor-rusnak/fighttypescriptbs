import { getFighters } from './services/fightersService';
import { createFighters } from './fightersView';
import { Fighter } from './models/fighter';

const rootElement = document.getElementById('root');
const loadingElement = document.getElementById('loading-overlay');

export async function startApp() {
  try {
    startLoading();

    const fighters = await getFighters();
    const fightersElement = createFighters(<Fighter[]>fighters);

    rootElement.appendChild(fightersElement);
  } catch (error) {
    console.warn(error);
    rootElement.innerText = 'Failed to load data';
  } finally {
    stopLoading();
  }
}
export function startLoading() {
  loadingElement.style.visibility = 'visible';
}

export function stopLoading() {
  loadingElement.style.visibility = 'hidden';
}

export function lockCheckboxes() {
  const checkboxes = getCheckboxesFromFighters();
  checkboxes.forEach((checkbox,index,array)=> {
    checkbox.setAttribute('disabled','');
  });
}

export function unlockCheckboxes() {
  const checkboxes = getCheckboxesFromFighters();
  checkboxes.forEach((checkbox,index,array)=> {
    checkbox.removeAttribute('disabled');
  });
}

function getCheckboxesFromFighters() {
  const htmlElements = rootElement.getElementsByClassName('custom-checkbox');
  const elements = Array.from(htmlElements).map((el)=>Array.from(el.getElementsByTagName('input'))[0]);
  return elements;
}