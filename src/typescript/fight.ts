import { createElement } from './helpers/domHelper';
import { hideModal, showModal } from './modals/modal';
import { Fighter } from './models/fighter';

const timer = (ms: number) => new Promise(res => setTimeout(res, ms));

export async function fight(firstFighter: Fighter, secondFighter: Fighter): Promise<Fighter> {
  let leftFighter: Fighter = { ...firstFighter };
  let rightFighter: Fighter = { ...secondFighter };

  const winner = await startFighting(leftFighter, rightFighter);

  return winner;
}

function createFightLogBody(stopFightEvent:Function){
  const body = createElement('div', 'fight-log');
  const stopButton = createElement('button', 'fight-stop-btn');

  stopButton.innerText = "Finish with draw";
  stopButton.addEventListener('click', (ev) => stopFightEvent());

  body.append(stopButton);
  return body;
}

async function startFighting(first: Fighter, second: Fighter) {
  let isFightStopped: boolean = false;
  const stopFight:Function = (ev:Event) => isFightStopped = true;

  const body = createFightLogBody(stopFight);

  showModal('Fighting', body, false);
  try {
    while (first.health > 0 && second.health > 0) {

      const hit = getDamage(first, second);
      
      updateFightInfo(body, first, second, hit);
      await timer(1000);

      if (isFightStopped)
        throw new Error("The fight was stopped");

      [first, second] = [second, first];
    }

    if (first.health > 0) 
      return first;
    else 
      return second;

  } catch (error) {
    throw error;
  } finally {
    hideModal();
  }
}

function updateFightInfo(body: HTMLElement, first: Fighter, second: Fighter, hit: number) {
  const element = createElement('p', 'fight-log-item');
  element.innerText = `${first.name} hits ${second.name} with ${hit} of damage, left ${second.health} hp`;
  body.append(element);
}

export function getDamage(attacker: Fighter, enemy: Fighter): number {
  let damage = getHitPower(attacker) - getBlockPower(enemy);

  if (damage < 0) damage = 0;

  enemy.health -= damage;
  return damage;
}

export function getHitPower(fighter: Fighter): number {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter: Fighter): number {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
